CREATE TABLE accounts (
    id BIGSERIAL,
    login CHARACTER VARYING,
    password CHARACTER VARYING,
    PRIMARY KEY(id)
);

CREATE TABLE users (
    account_id BIGSERIAL,
    family CHARACTER VARYING,
    name CHARACTER VARYING,
    CONSTRAINT fk_account
        FOREIGN KEY(account_id)
            REFERENCES accounts(id)
);

CREATE TABLE bank (
    account_id BIGSERIAL,
    balance FLOAT,
    CONSTRAINT fk_account
        FOREIGN KEY(account_id)
            REFERENCES accounts(id)
);

INSERT INTO accounts(id, login, password) VALUES
(1, 'road_knight', 'qwerty'),
(2, 'kyyyyle', 'ginger_rulezzz'),
(3, 'big_muscled_man', 'iHateKyle'),
(4, 'boobs_inspector', 'boooooooooobs'),
(5, 'alcoman', '123456'),
(6, 'bigmom', 'EricIsMyPie');

INSERT INTO users(account_id, name, family) VALUES
(1, 'Stan', 'Marsh'),
(2, 'Kyle', 'Broflovski'),
(3, 'Eric', 'Cartman'),
(4, 'Kenny', 'McCormick'),
(5, 'Randy', 'Marsh'),
(6, 'Liane', 'Cartman');

INSERT INTO bank(account_id, balance) VALUES
(1, 200),
(2, 30000),
(3, 50),
(4, 8),
(5, 3000),
(6, 200);