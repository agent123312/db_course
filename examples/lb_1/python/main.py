from fastapi import FastAPI
import psycopg2
import json
import string
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

app = FastAPI()

try:
    # пытаемся подключиться к базе данных
    conn = psycopg2.connect('postgresql://postgres_user:postgres@localhost:5432/postgres_db')
except:
    # в случае сбоя подключения будет выведено сообщение  в STDOUT
    print('Can`t establish connection to database')

@app.get("/api/sp/characters")
def getCharacters():
    global conn
    cursor = conn.cursor()    
    cursor.execute('SELECT id, name, family, birth FROM sp_characters')
    result = cursor.fetchall()
    r = {}
    res = []
    for i in range(0, len(result)):
        res.append({
            'id': result[i][0],
            'name': result[i][1],
            'family': result[i][2],
            'birth': result[i][3],
        }) 
    json_compatible_item_data = jsonable_encoder(res)
    return JSONResponse(content=json_compatible_item_data)

@app.get("/api/sp/characters/{id}")
def getCharacter(id: int):
    global conn
    cursor = conn.cursor()    
    cursor.execute('SELECT id, name, family, birth FROM sp_characters WHERE id = %s', (id,))
    result = cursor.fetchall()
    r = {}
    res = []
    for i in range(0, len(result)):
        res.append({
            'id': result[i][0],
            'name': result[i][1],
            'family': result[i][2],
            'birth': result[i][3],
        }) 
    json_compatible_item_data = jsonable_encoder(res)
    return JSONResponse(content=json_compatible_item_data)

@app.post("/api/sp/characters")
def createCharacter(name, family, birth):
    global conn
    cursor = conn.cursor()  

    cursor.execute('INSERT INTO sp_characters(name, family, birth) VALUES (%s, %s, %s) RETURNING id', (name, family, birth))
    result = cursor.fetchall()
    res = {
        'id': result[0][0],
        'name': name,
        'family': family,
        'birth': birth,
    }
    conn.commit()
    json_compatible_item_data = jsonable_encoder(res)
    return JSONResponse(content=json_compatible_item_data)

@app.put("/api/sp/characters/{id}")
def updateCharacter(id: int, name, family, birth):
    global conn
    cursor = conn.cursor()  

    cursor.execute('UPDATE sp_characters SET name = %s, family = %s, birth = %s WHERE id = %s', (name, family, birth, id))
    conn.commit()
    cursor.execute('SELECT id, name, family, birth FROM sp_characters WHERE id = %s', (id,))
    result = cursor.fetchone()
    print(result)
    res = {
        'id': result[0],
        'name': result[1],
        'family': result[2],
        'birth': result[3],
    }
    json_compatible_item_data = jsonable_encoder(res)
    return JSONResponse(content=json_compatible_item_data)

@app.delete("/api/sp/characters/{id}")
def deleteCharacters(id: int):
    global conn
    cursor = conn.cursor()  

    cursor.execute('DELETE FROM sp_characters WHERE id = %s', (id,))
    
    res = cursor.rowcount
    conn.commit()
    json_compatible_item_data = jsonable_encoder(res)
    return JSONResponse(content=json_compatible_item_data)