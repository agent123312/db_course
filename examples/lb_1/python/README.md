# Подготовка
## Рекомендуется установить виртульное окружение (например, virtualenv)

# Необходимые пакеты
```
pip install fastapi
pip install psycopg2-binary
pip install uvicorn
```

# Запуск
```
uvicorn main:app --reload
```