package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"io/ioutil"
	"os"
	//"strconv"
	//"strings"
	"time"
)

var err error

func handleError(err error) {
	if err != nil {
		str := fmt.Errorf("error occured at %v, %s", time.Now(), err)
		panic(str)
	}
}

func readConfig() {
	//var data []byte
	data, err := os.Open("conf.json")
	handleError(err)
	bytedata, err := ioutil.ReadAll(data)
	handleError(err)
	err = json.Unmarshal(bytedata, &config)
	handleError(err)
}

func dbConnect() {
	db, err = sql.Open("postgres", config.DBConnection)
	handleError(err)
}