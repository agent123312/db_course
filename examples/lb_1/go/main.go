package main

import (
	"encoding/json"
	"log"
	"net/http"
	"database/sql"
	"github.com/gorilla/mux"
)

var config ConfigStruct

var db *sql.DB

func getCharacters(w http.ResponseWriter, r *http.Request) {
	query := "SELECT id, name, family, birth FROM sp_characters"
	rows, err := db.Query(query)
	handleError(err)
	var data []SpCharacter
	var (
		v_Id string
		v_Name string
		v_Family string
		v_Birth string
	)
	for rows.Next() {
		if err := rows.Scan(
			&v_Id,
			&v_Name,
			&v_Family,
			&v_Birth,
		); err != nil {
			log.Fatal(err)
		}
		d := SpCharacter{
			 Id : v_Id,
			 Name : v_Name,
			 Family : v_Family,
			 Birth : v_Birth,
		}
		data = append(data, d)
	}
	err = rows.Close()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func getCharacter(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)

	query := "SELECT id, name, family, birth FROM sp_characters WHERE id = $1"
	rows, err := db.Query(query, params["id"])
	handleError(err)
	var data []SpCharacter
	var (
		v_Id string
		v_Name string
		v_Family string
		v_Birth string
	)
	for rows.Next() {
		if err := rows.Scan(
			&v_Id,
			&v_Name,
			&v_Family,
			&v_Birth,
		); err != nil {
			log.Fatal(err)
		}
		d := SpCharacter{
			 Id : v_Id,
			 Name : v_Name,
			 Family : v_Family,
			 Birth : v_Birth,
		}
		data = append(data, d)
	}
	err = rows.Close()
	json.NewEncoder(w).Encode(data)
}

func createCharacter(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var data SpCharacter
	var id string
	_ = json.NewDecoder(r.Body).Decode(&data)
	query := "INSERT INTO sp_characters(name, family, birth) VALUES ($1, $2, $3) RETURNING id"
	stmt, err := db.Prepare(query)
	handleError(err)
	err = stmt.QueryRow(data.Name, data.Family, data.Birth).Scan(&id)
	handleError(err)
	data.Id = id
	
	json.NewEncoder(w).Encode(data)
}

func updateCharacter(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id := params["id"]
	var data SpCharacter
	_ = json.NewDecoder(r.Body).Decode(&data)
	query := "UPDATE sp_characters SET name = $1, family = $2, birth = $3 WHERE id = $4"
	stmt, err := db.Prepare(query)
	handleError(err)
	_, err = stmt.Exec(data.Name, data.Family, data.Birth, id)
	handleError(err)
	data.Id = id
	
	json.NewEncoder(w).Encode(data)
}

func deleteCharacter(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)

	id := params["id"]
	query := "DELETE FROM sp_characters WHERE id = $1"
	stmt, err := db.Prepare(query)
	handleError(err)
	res, err := stmt.Exec(id)
	affected, err := res.RowsAffected()

	json.NewEncoder(w).Encode(affected)
}

func main() {
	readConfig()
	dbConnect()
	defer db.Close()

	r := mux.NewRouter()

	r.HandleFunc("/api/sp/characters", getCharacters).Methods("GET")
	r.HandleFunc("/api/sp/characters/{id}", getCharacter).Methods("GET")
	r.HandleFunc("/api/sp/characters", createCharacter).Methods("POST")
	r.HandleFunc("/api/sp/characters/{id}", updateCharacter).Methods("PUT")
	r.HandleFunc("/api/sp/characters/{id}", deleteCharacter).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":5640", r))
}