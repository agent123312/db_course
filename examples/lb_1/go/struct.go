package main

type ConfigStruct struct {
	ListenPort   string `json:"ListenPort"`
	DBConnection string `json:"DBConnection"`
}

type SpCharacter struct {
	Id     string  `json:"id`
	Name   string  `json:"name"`
	Family  string  `json:"family"`
	Birth string `json:"birth"`
}