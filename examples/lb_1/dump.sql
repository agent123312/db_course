CREATE TABLE sp_characters (
    id SERIAL PRIMARY KEY,
    family CHARACTER VARYING,
    name CHARACTER VARYING,
    birth CHARACTER VARYING
);

INSERT INTO sp_characters(name, family, birth) VALUES 
('Stan', 'Marsh', 'XXXX-10-19'),
('Eric', 'Cartman', 'XXXX-07-01'),
('Kenny', 'McCormick', 'XXXX-03-22'),
('Kyle', 'Broflovski', 'XXXX-05-26'),
('Wendy', 'Testaburger', 'XXXX-XX-XX');