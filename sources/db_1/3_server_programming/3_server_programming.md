---
header: 'Серверное программирование'
author: 'Дерюгин Д.Ф.'
_footer: 'Улан-Удэ, БГУ, 2023'
title: Базы данных. Серверное программирование
# description: Hosting Marp slide deck on the web
paginate: true
theme: default
_paginate: false

---

<!-- _class: lead -->

## Базы данных

# Серверное программирование

#### Дерюгин Д.Ф. 
#### ст. преп. ИСМИ, ИМФКН, БГУ

---
<!-- header: 'Серверное программирование / Расширение SQL' -->
# Расширение SQL

- функции
- агрегатные функции
- типы данных
- операторы
- классы операторов для индексов
- пакеты связанных объектов

---
<!-- header: 'Серверное программирование / Расширение SQL / Системы типов PostgreSQL' -->
# Системы типов PostgreSQL
* базовые типы
* - типы ниже SQL (integer, string, ...)
* типы-контейнеры
* - массивы, составные типы, диапазоны
* домены
* псевдотипы
* полиморфные типы

---
<!-- header: 'Серверное программирование / Расширение SQL / Пользовательские функции' -->
# Пользовательские функции
- функции на языке запросов (SQL)
- функции на процедурных языках
- внутренние функции
- функции на языке C

---
# Процедурные языки
Диалекты:
- PL/SQL - Oracle
- T-SQL - MSSQL, Microsoft
- PL/pgSQL - PostgreSQL
* ...

---
# Процедурные языки в PostgreSQL:
* PL/Tcl
* PL/Perl
* PL/Python
* PL/PHP
* PL/Ruby
* PL/Java
* ...

* ## Мы рассмотрим PL\pgSQL

---
<!-- header: 'Серверное программирование / Хранимые процедуры и функции' -->
# Хранимые процедуры и функции
Выполняют произвольный список операторов SQL

* Функции возвращают результат последнего запроса 
* Процедуры - ничего не возвращают

Stored procedure
Routine

---
# Преимущества и недостатки

Преимущества: 
* Повторное использование кода
* Разделение логики
* Сокращение сетевого трафика
* Изоляция пользователей от таблиц

Недостатки:
* Повышение нагрузки на сервер
* Дублирование логики
* Сложность миграции на другую СУБД

---
Объявление функции:
```sql
-- Hello, World!
CREATE FUNCTION hello_world() RETURNS varchar AS $$
    SELECT 'Hello, World!' as hello;
$$ LANGUAGE SQL;
```
Вызов функции:
```sql
SELECT hello_world();
```

---
Объявление процедуры:
```sql
-- Hello, World!
CREATE PROCEDURE hello_world() AS $$
    SELECT 'Hello, World!' as hello;
$$ LANGUAGE SQL;
```
Вызов процедуры
```sql
CALL hello_world();
```

---
<!-- header: 'Серверное программирование / Хранимые функции' -->
# Создание хранимой функции
```sql
CREATE FUNCTION somefunc([param[, …]]) RETURNS return_type
AS 'routine_body'
LANGUAGE plpgsql;
```
либо
```sql
CREATE FUNCTION somefunc([param[, …]]) RETURNS return_type
AS $$ 
    routine_body
$$ LANGUAGE plpgsql;
```
Вызов функции:
```sql
SELECT somefunc([param[, …]])
```

---
# routine_body - блок
```sql
[ <<метка>>]
[ DECLARE
    объявления переменных
]
BEGIN
    операторы
END [ <<метка>>];
```
Блоки могут быть вложенными

---
```sql
CREATE FUNCTION somefunc() RETURNS integer AS $$
<< outerblock >>
DECLARE 
    test_val integer := 30;
BEGIN 
    RAISE NOTICE 'test_val = %', test_val;
    test_val := 50;
    DECLARE 
        test_val integer := 80;
    BEGIN
        RAISE NOTICE 'Внутри блока test_val = %', test_val;
        RAISE NOTICE 'Во внешнем блоке test_val = %', outerblock.test_val;
    END;
    RAISE NOTICE 'А сейчас test_val = %', test_val;
    RETURN test_val;
END;
$$ LANGUAGE plpgsql;
```

---
# Объявления переменных
```sql
имя [ CONSTANT ] 
    тип [ COLLATE правила_сортировки ] 
    [ NOT NULL ] 
    [ { DEFAULT | := | = } выражение ];
```
```sql
user_id integer;
cnt integer DEFAULT 20;
family varchar := 'Some Family'
first_name varchar = 'Some Name'
tr_time CONSTANT timestamp := now();

x integer := user_id * 100 + 1
```

---
# Параметры функции
```sql
CREATE FUNCTION square(num real) RETURNS real AS $$
BEGIN
    RETURN num*num;
END;
$$ LANGUAGE plpgsql;
```

---
```sql
CREATE FUNCTION square(
    num real, 
    OUT result real
) RETURNS real AS $$
BEGIN
    result := num*num;
END;
$$ LANGUAGE plpgsql;
```

---
Имена аргументов можно опустить
```sql
CREATE FUNCTION sum_int(
    integer, 
    integer
) RETURNS integer AS $$
    SELECT $1 + $2;
$$ LANGUAGE SQL;
```

---
# Возврат нескольких значений
```sql
CREATE FUNCTION square(
    num real, 
    OUT square_val real, 
    OUT cube_val
) AS $$
BEGIN
    square_val := num*num;
    cube_val := num*num*num;
END;
$$ LANGUAGE plpgsql;
```
```sql
SELECT square(3.0);
```

---
```sql
CREATE FUNCTION get_family_name(
    r_t sp_characters
) RETURNS real AS $$
BEGIN
    RETURN r_t.name 
        || ' ' 
        || r_t.family;
END;
$$ LANGUAGE plpgsql;
```
* Создание анонимного типа record
* Для процедур указать ВСЕ параметры - притом переменные

---
Покупка со счета
```sql
CREATE FUNCTION buy_something(
    account_id integer, 
    price numeric
) RETURNS numeric AS $$
    UPDATE bank SET balance = balance - price
    WHERE bank.account_id =  account_id
    RETURNING balance;
$$ LANGUAGE SQL;
```
* Если тип возращаемого значения последнего SELECT не совпадает с типом возращаемого значения функции, то будет попытка приведения типов

---
# Функции со сложными типами
Передача строк таблицы
```sql
CREATE FUNCTION dreams(
    bank
) RETURNS numeric AS $$
    SELECT $1.balance * 1000000;
$$ LANGUAGE SQL;
```
```sql
SELECT users.name, users.family, dreams(bank.*) 
FROM bank
INNER JOIN users ON users.account_id = bank.account_id
```

---
# Выходные параметры
```sql
CREATE FUNCTION sleep_balance(
    IN account_id integer, 
    OUT dream_balance float, 
    OUT nightmare_balance float
) as $$
    SELECT balance*100 as dream_balance, balance/100 as nightmare_balance
    FROM bank
    WHERE bank.account_id = account_id;
$$ LANGUAGE SQL
```
```sql
SELECT sleep_balance(1);
```
* Возвращается составной тип

---
# Захват вывода
```sql
-- внутри какой-нибудь функции
-- ...
cnt integer DEFAULT 0
SELECT COUNT(*) INTO cnt FROM sp_characters;
-- делаем что-то с cnt
-- ...
```
* Только первая строка результирующей выборки
* Можно каждый столбец в отдельную переменную
* Для INSERT, DELETE, UPDATE - аналогично

---
# Выполнение динамически формируемых команд
```sql
EXECUTE строка-команды 
    [ INTO [ STRICT ] цель ] 
    [ USING выражение [, ... ] ];
```
Со STRICT команда должна вернуть ровно 1 строку, иначе - ошибка 

---
Пример
```sql
EXECUTE 'SELECT 
    COUNT(*) 
    FROM sp_characters 
    WHERE 
        birth LIKE $1 
        AND LENGTH(name) <= $2' 
INTO c 
USING 'XXXX-XX-XX', 4;
```

---
Если нужно варьровать таблицу или столбцы
```sql
tbl_name varchar DEFAULT 'sp_characters';
EXECUTE 'SELECT 
    COUNT(*) 
    FROM' || quote_ident(tabl_name) || 
    ' WHERE birth LIKE $1 AND LENGTH(name) <= $2' 
INTO c 
USING 'XXXX-XX-XX', 4;
```
Либо
```sql
tbl_name varchar DEFAULT 'sp_characters';
EXECUTE format('SELECT COUNT(*) FROM %I '
' WHERE birth LIKE $1 AND LENGTH(name) <= $2', tbl_name)
INTO c 
USING 'XXXX-XX-XX', 4;
```
* строки в format будут сконкатенированы автоматически, т.к. их разделяет перевод строки

---
Экранирование одинарных кавычек
```sql
EXECUTE 'UPDATE sp_characters SET '
        || quote_ident(colname) --для столбцов/таблиц
        || ' = '
        || quote_literal(newvalue) --для значений
        || ' WHERE id = '
        || quote_literal(keyvalue);
```
Если пустая строка должна быть NULL в запросе - вместо quote_literal используем quote_nullable

---
# Категории изменчивости функций
Категория детерминированности

* VOLATILE - абсолютно недетерминированная функция, может модифицировать базу данных
* STABLE - не может модифицировать базу, детерминирована
* IMMUTABLE - не может модифицировать базу, абсолютно детерминирована

* (!) При кэшировании плана выполнения:
* - STABLE вычисляется каждый раз при построении плана
* - IMMUTABLE заменяется константой при планировании

---
<!-- header: 'Серверное программирование / Управляющие структуры' -->
# Возвращение значения из функции
* RETURN
* - возвращение значения из функции, выполнение останавливается
* RETURN NEXT
* - добавляет строки в результирующее множество, выполнение неостанавливается
* RETURN QUERY
* - = RETURN NEXT
* RETURN QUERY EXECUTE
* - выполнить команду для добавления в результирующее множество

---
<!-- header: 'Серверное программирование / Хранимые процедуры и функции / Управляющие структуры / Условный оператор' -->
# Условный оператор
```sql
IF логическое_выражение THEN 
    операторы
END IF;
```
```sql
IF num > 0 THEN 
    result = num*2;
END IF;
```

---
```sql
IF логическое_выражение THEN 
    операторы
ELSE 
    операторы
END IF;
```
```sql
IF num > 0 THEN 
    result = num*2;
ELSE 
    result = -num*2;
END IF;
```

---
```sql
IF логическое_выражение THEN 
    операторы
[ ELSIF логическое_выражение THEN 
    операторы [ ELSIF логическое_выражение THEN 
    операторы ... ]]
[ ELSE операторы ]
END IF;
```
```sql
IF num > 0 THEN 
    result = num*2;
ELSIF num <> 0 
    result = -num*2;
ELSE 
    result = num*10;
END IF;
```

---
```sql
CASE выражение_поиска 
    WHEN выражение [, выражение [...]] THEN операторы
    [ WHEN выражение [, выражение [...]] THEN операторы ]
    [ ELSE операторы ]
END CASE;
```
```sql
CASE x
    WHEN 1,2,3,4,5,6,7,8,9,10 THEN y := 5;
    WHEN 11,12,13,14,15,16,17,18,19,20 THEN y := 10;
    ELSE y := -1;
END CASE;
```

---
```sql
CASE 
    WHEN логическое_выражение THEN операторы
    [ WHEN логическое_выражение THEN операторы ]
    [ ELSE операторы ]
END CASE;
```
```sql
CASE 
    WHEN x BETWEEN 0 AND 10 THEN y := 5;
    WHEN x BETWEEN 11 AND 20 THEN y := 10;
    ELSE y := -1;
END CASE;
```

---
<!-- header: 'Серверное программирование / Хранимые процедуры и функции / Управляющие структуры / Простые циклы' -->
# Циклы
```sql
[<<метка>>]
LOOP
    операторы
END LOOP [ метка ];
```

---
```sql
LOOP 
    x := x + 1
    IF x = 10 THEN 
        EXIT;
    END IF;
END LOOP;
```
```sql
LOOP 
    x := x + 1
    EXIT WHEN x = 10;
END LOOP;
```

---
```sql
LOOP 
    x := x + 1
    EXIT WHEN x = 10;
    CONTINUE WHEN x < 5;
    -- вычисления для x <5
END LOOP;
```
```sql
LOOP 
    x := x + 1
    EXIT WHEN x = 10;
    IF x >= 5 THEN
        CONTINUE;
    END IF;
END LOOP;
```

---
```sql
[<<метка>>]
WHILE логическое_выражение LOOP
    операторы
END LOOP [ метка ]
```
```sql
WHILE x < 100 LOOP 
    s := s + x;
END LOOP;
```

---
```sql
[<<метка>>]
FOR имя IN [ REVERSE ] выражение .. выражение [ BY выражение ] LOOP
    операторы
END LOOP [ метка ]
```
```sql
FOR x IN 1..10 LOOP
    s := s + x;
END LOOP;
```
```sql
FOR x IN REVERSE 10..1 LOOP
    s := s + x;
END LOOP;
```
```sql
FOR x IN REVERSE 10..1 BY 2 LOOP
    s := s + x;
END LOOP;
```

---
```sql
[<<метка>>]
FOR цель IN запрос LOOP
    операторы
END LOOP [ метка ]
```
```sql
DECLARE guys RECORD;
-- ...
FOR guys IN SELECT * FROM sp_characters LOOP
    RAISE NOTICE guys.id || ' ' || guys.name || ' ' || guys.family;
-- ...
```

---
```sql
[<<метка>>]
FOR цель IN EXECUTE выражение_проверки [ USING выражение [ , ... ] ] LOOP
    операторы
END LOOP [метка];
```

---
```sql
[<<метка>>]
FOREACH цель [ SLICE число ] IN ARRAY выражение LOOP
    операторы
END LOOP [метка];
```
```sql
CREATE FUNCTION sum(int[]) RETURNS int8 AS $$
DECLARE 
    s int8 := 0;
    x int;
BEGIN 
    FOREACH x IN ARRAY $1 LOOP
        s := s + x;
    END LOOP;
    RETURN s;
END;
$$ LANGUAGE pgplsql;
```

---
```sql
CREATE FUNCTION scan_rows(int[]) RETURNS int8 AS $$
DECLARE 
    s int8 := 0;
    x int;
BEGIN 
    FOREACH x SLICE 1 IN ARRAY $1 LOOP
        RAISE NOTICE x;
    END LOOP;
END;
$$ LANGUAGE pgplsql;
```

---
<!-- header: 'Серверное программирование / Хранимые процедуры и функции / Управляющие структуры / Обработка ошибок' -->
# Обработка ошибок
```sql
[<<метка>>]
[ DECLARE 
    объявления ]
BEGIN
    операторы
EXCEPTION 
    WHEN условие [ OR условие ] THEN 
        операторы
    [ WHEN условие [ OR условие ] THEN 
        операторы
    ... ]
END; 
```
Условия и коды ошибок (https://postgrespro.ru/docs/postgresql/15/errcodes-appendix)  

---
```sql
-- ...
FOR x IN REVERSE 10..-10 LOOP
    s := s / x;
    EXCEPTION 
        WHEN division_by_zero THEN RAISE NOTICE 'Деление на 0';
END LOOP;
-- ...
```

---
# Получение информации об ошибке
```sql
GET STACKED DIAGNOSTICS переменная { = | := } элемент [, ... ];
```
Элементы:
* RETURNED_SQLSTATE - код исключения
* COLUMN_NAME - имя столбца, относящегося к исключению
* CONSTRAINT_NAME
* MESSAGE_TEXT
* ...

---
```sql
-- ...
EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS text_var1 = MESSAGE_TEXT,
        text_var2 = PG_EXCEPTION_DETAIL,
        text_var3 = PG_EXCEPTION_HINT;
-- ...
```

---
<!-- header: 'Серверное программирование / Курсоры' -->
# Курсоры
Указатель
```sql
имя [ [ NO ] SCROLL ] CURSOR [ { аргументы } ] FOR запрос; 
```

SCROLL - можно прокручивать назад
NO SCROLL - нельзя
аргумент - пара "имя тип_данных"

---
```sql
DECLARE
    curs1 refcursor; -- несвязанный курсор
    curs2 CURSOR FOR 
        SELECT * 
        FROM tbl1;
    curs3 CURSOR  (key integer) FOR 
        SELECT * 
        FROM tbl1 
        WHERE col1 = key;
```

---
Открытие курсора
```sql
OPEN несвязанный_курсор 
    [ [ NO ] SCROLL ] 
    FOR запрос;
OPEN несвязанный_курсор 
    [ [ NO ] SCROLL ] 
    FOR EXECUTE строка_запроса 
    [ USING выражение [, ...]];
```

---
```sql
OPEN связанный_курсор 
    [ ( [имя_аргумента := ] значение_аргумента [, ...])];
OPEN связанный_курсор 
    [ [ NO ] SCROLL ] 
    FOR EXECUTE 
        строка_запроса [ USING выражение [, ...]];
```
```sql
OPEN curs2;
OPEN curs3(42);
OPEN curs(key := 42);
```

---
```sql
DECLARE
    key integer;
    curs4 CURSOR 
    FOR SELECT * 
        FROM tbl1 
        WHERE col1 = key;
BEGIN
    key := 42;
    OPEN curs4;
END;
```

---
Работа с курсорами
```sql
FETCH [ направление { FROM | IN }] курсор INTO цель;
```
Извлечение строки
Направление: NEXT, PRIOR, FIRST, LAST, ABSOLUTE число, RELATIVE число, FORWARD, BACKWARD
```sql
FETCH curs1 INTO rowvar;
FETCH curs2 INTO foo, bar, baz;
FETCH LAST FROM curs3 INTO x, y;
FETCH RELATIVE -2 FROM curs4 INTO x;
```

---
```sql
MOVE [ направление { FROM | IN }] курсор;
```
Перемещает курсор без извлечения данных

---
```sql
UPDATE tbl SET ... WHERE CURRENT OF курсор;
DELETE FROM tbl WHERE CURRENT OF курсор;
```
Изменение/удаление строки, на которую указывает курсор

---
```sql
CLOSE курсор;
```
Закрыть курсор

* Курсоры можно возвращать/передавать в функции

---
<!-- header: 'Серверное программирование / Триггеры' -->
# Триггеры
Триггер - указание, что база данных должна автоматически выполнить заданную функцию всякий раз, когда выполнен определенный тип операции

* Работают с DML операциями в рамках таблицы

* Событие -> триггерная функция
* Одна триггерная функция - много событий
* На событие может быть много триггерных функций. Вызов осуществляется в алфавитном порядке

---
## Виды триггеров: 
* - операторный
* - - вызывается один раз при выполнении оператора
* - построчный
* - - вызывается один для каждой строки, затронутой оператором

---
Событие - срабатывание оператора INSERT, UPDATE, DELETE, TRUNCATE

* Виды событий:
* - BEFORE
* - AFTER
* - INSTEAD OF

---
# Видимость изменений в данных
* Операторные: 
* - BEFORE - не видны изменения
* - AFTER - видны все изменения
* Построчные: 
* - BEFORE - не видны изменения текущей строки, видны предыдущих
* - AFTER - видны все изменения

---
```sql
CREATE [ OR REPLACE ] [ CONSTRAINT ] 
    TRIGGER имя { BEFORE | AFTER | INSTEAD OF } 
        { событие [ OR ... ] }
ON имя_таблицы
[ FROM ссылающаяся_таблица ]
[ NOT DEFERABLE | [ DEFERABLE ] [INITIALLY IMMEDIATE | INITIALLY DEFERRED ] ]
[ REFERENCING { { OLD | NEW } TABLE [ AS ] имя_переходного_отношения } [ ... ] ]
[ FOR [ EACH ] { ROW | STATEMENT } ]
[ WHEN { условие }]
EXECUTE { FUNCTION | PROCEDURE } имя_функции(аргументы)
```

---
<!-- header: 'Серверное программирование / Триггеры / Триггеры событий' -->
# Триггеры событий
Работают с DDL операциями в рамках базы данных

* Те же триггеры, но немного с другим примененем

---
## События: 
* ddl_command_start
* - перед CREATE, ALTER, DROP, SECURITY LABEL, COMMENT, GRANT, REVOKE, ...
* ddl_command_end
* - после CREATE, ALTER, DROP, SECURITY LABEL, COMMENT, GRANT, REVOKE, ...
* sql_drop
* - перед ddl_command_end для команд, удаляющих объект из базы данных
* table_rewrite
* - после перезаписи таблицы командами ALTER TABLE, ALTER TYPE

---
```sql
CREATE EVENT TRIGGER имя 
ON событие
[ WHEN переменная_фильтра IN ( значение_фильтра [ , ... ] ) [ AND ... ] ]
EXECUTE { FUNCTION | PROCEDURE } имя_функции()
```

---
<!-- header: 'Серверное программирование / Триггеры / Триггерные функции' -->
# Триггерные функции
Объявляется без аргументов, возвращает trigger

* Переменные:
* - NEW - RECORD, новая (измененная) строка для построчного триггера
* - OLD - RECORD, старая строка для построчного триггера
* - TG_NAME - имя триггера
* - TG_WHEN - BEFORE | AFTER | INSTEAD OF
* - TG_LEVEL - ROW | STATEMENT
* - TG_OP - INSERT | UPDATE | DELETE | TRUNCATE
* - TG_TABLE_NAME
* - TG_TABLE_SCHEMA
* - ...

---
```sql
CREATE TABLE emp (
    empname text,
    salary integer,
    last_date timestamp,
    last_user text
);
```

---
```sql
CREATE FUNCTION emp_stamp() RETURNS trigger AS $emp_stamp$
BEGIN
    IF NEW.empname IS NULL THEN
        RAISE EXCEPTION 'empname cannot be null';
    END IF;
    IF NEW.salary IS NULL THEN
        RAISE EXCEPTION '% cannot have null salary', NEW.empname;
    END IF;
    IF NEW.salary < 0 THEN
        RAISE EXCEPTION '% cannot have a negative salary', NEW.empname;
    END IF;
    NEW.last_date := current_timestamp;
    NEW.last_user := current_user;
    RETURN NEW;
END;
$emp_stamp$ LANGUAGE plpgsql;
```
```sql
CREATE TRIGGER emp_stamp BEFORE INSERT OR UPDATE ON emp
FOR EACH ROW EXECUTE FUNCTION emp_stamp();
```

---
# Событийные триггерные функции
Объявляется без аргументов, возвращает event_trigger
* Переменные:
* - TG_EVENT - событие
* - TG_TAG - тег команды

---
```sql
CREATE OR REPLACE FUNCTION snitch() RETURNS event_trigger AS $$
BEGIN
    RAISE NOTICE 'snitch: % %', tg_event, tg_tag;
END;
$$ LANGUAGE plpgsql;
```
```sql
CREATE EVENT TRIGGER snitch ON ddl_command_start EXECUTE FUNCTION snitch();
```

---
<!-- header: 'Серверное программирование / Представления' -->
# Представления
Представление (VIEW) - объект базы 	данных, являющийся результатом 	выполнения SELECT запроса к базе данных


«Виртуальная таблица»

---
# Преимущества
* гибкая настройка прав
* изоляция пользователей от данных
* разделение логики хранения данных и ПО

* Могут быть основаны на других представлениях, включать в себя множество таблиц
---
```sql
CREATE [ OR REPLACE ] 
    [ TEMP | TEMPORARY ] 
    [ RECURSIVE ] 
    VIEW имя [ ( имя_столбца [ , ... ] ) ]
[ WITH (имя_параметра_представления 
    [ = значение_параметра_представления ] 
    [, ... ] ) ]
AS запрос
[ WITH [ CASCADED | LOCAL ] CHECK OPTION ]
```

* запрос выполняется каждый раз при обращении к представлению

---
# Параметры
* TEMPORARY | TEMP - временное, до конца сеанса
* RECURSIVE - рекурсивное представление
* имя_параметра_представления:
* - check_option(enum) - local | cascaded
* - security_barrier(boolean) - защита на уровне строк
* - security_invoker(boolean) - проверка прав пользователя
* WITH CHECK OPTION
* - проверка измененных строк на соответствие условию представления
* - - LOCAL - только в самом представлении
* - - CASCADED - во всех нижележащих

---
## security_barrier(boolean)
```sql
CREATE VIEW phone_number AS
SELECT person, phone FROM phone_data WHERE phone NOT LIKE '412%';
```
* Небезопасное представление
```sql
CREATE FUNCTION tricky(text, text) RETURNS bool AS $$
BEGIN
    RAISE NOTICE '% => %', $1, $2;
RETURN true;
END;
$$ LANGUAGE plpgsql COST 0.0000000000000000000001;
```
```sql
SELECT * FROM phone_number WHERE tricky(person, phone);
```
* Получили данные представления в сообщениях об ошибке за счет более дешевой в исполнении функции tricky

---
## security_invoker(boolean)
- false
* - доступ к нижележащим отношениям с правами владельца представления
- true
* - доступ к нижележащим отношениям с правами вызывающего

---
# Изменяемые представления
* Требования: 
* - 1 отношение во FROM (таблица, изменяемое представление)
* - определение не содержит: WITH, DISTINCT, GROUP BY, HAVING, LIMIT, OFFSET 
* - определение не содержит операций со множествами: UNION, INTERSECT, EXCEPT
* - список выборки не содержит агрегатные, оконные функции и функции, возвращающие множества

* Можно эмулировать изменяемость за счет триггеров

---
```sql
CREATE VIEW comedies AS
    SELECT *
    FROM films
    WHERE kind = 'Comedy';
```
```sql
CREATE VIEW pg_comedies AS
    SELECT *
    FROM comedies
    WHERE classification = 'PG'
WITH CASCADED CHECK OPTION;
```
```sql
CREATE RECURSIVE VIEW public.nums_1_100 (n) AS
    VALUES (1)
UNION ALL
    SELECT n+1 FROM nums_1_100 WHERE n < 100;
```

---
<!-- header: 'Серверное программирование / Материализованные представления' -->
# Материализованные представления
Заданный запрос выполняется и наполняет представление в момент вызова команды. 
* Позже набор данных можно обновить

---
```sql
CREATE MATERIALIZED VIEW [ IF NOT EXISTS ] имя_таблицы
    [ (имя_столбца [ , ... ] ) ] 
    [ USING метод ]
    [ WITH ( параметр_хранения [ = значение ] [ , ... ] ) ]
    [ TABLESPACE табличное_пространство ]
    AS запрос
    [ WITH [ NO DATA ] ]
```
## Параметры
* USING метод - метод доступа. Обычно TABLE
* параметр_хранения - также, как и у TABLE
* TABLESPACE - табличное пространство
* WITH [ NO DATA ] - наполнять ли данными представление

---
Обновление содержимого материализованного представления
```sql
REFRESH MATERIALIZED VIEW [ CONCURRENTLY ] имя
    [ WITH [ NO ] DATA ]
```
* CONCURRENTLY - не блокировать выборки в процессе обновления представления
* WITH NO DATA - освободит пространство, занятое материализованным представлением

---
<!-- header: 'Серверное программирование / Постскриптум' -->
# P.S.
Неразобранные вопросы:
- COLLATE 
- статус выполнения команды
- RAISE NOTICE
- перегрузка функций
- методы доступа (default_table_access_method)
- ...

* ## Подробности в Документации к PostgreSQL 
* pdf: https://postgrespro.ru/docs/
* web: https://postgrespro.ru/docs/postgresql/15/index
---
<!-- _class: lead -->

# Спасибо за внимание
## Вопросы?