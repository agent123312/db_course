---
header: 'Хранение иерархических структур'
author: 'Дерюгин Д.Ф.'
_footer: 'Улан-Удэ, БГУ, 2023'
title: Базы данных. Простые деревья
# description: Hosting Marp slide deck on the web
paginate: true
theme: default
_paginate: false

---

<!-- _class: lead -->

## Базы данных

# Простые деревья

#### Дерюгин Д.Ф. 
#### ст. преп. ИСМИ, ИМФКН, БГУ

---
# Цель

Хранение иерархических структур

- категории товаров (статей, ...);
- треды комментариев;
- классификаторы (КЛАДР, ОКВЭД, ...);
- организационные структуры;
- ...

---
<!-- header: 'Хранение иерархических структур / Примеры структур' -->
# Примеры структур
## Структура ИТ отдела
![bg contain](./images/tree_peoples.svg)

---
Структура Mail.ru Group (06.07.2021, https://journal.tinkoff.ru/news/review-mail-ru-group/)
![](./images/tree_mail.svg)

---
<!-- header: 'Хранение иерархических структур / Основные понятия' -->
# Основные понятия

Данные имеют рекурсивные связи
- каждая запись - 1 узел (1 предок, N потомков);
- узел без предка - корень;
- узел без потомков - лист;

---
# Основные операции
- получение прямого родителя
- получение всех родителей
- получение прямых потомков
- получение всех потомков
- вставка листа
- удаление узла
- удаление поддерева
- перемещение узла
- перемещение поддерева

Остальные возможные операции могут быть выражены через основные (причем основные операции **не являются базисом**)

---
<!-- header: 'Хранение иерархических структур / Используемый пример. Вольная классификация СУБД' -->
![bg contain](./images/tree_dbms.svg)

---
# Основные способы хранения
- список соседства;
- перечисление путей;
- вложенные множества;
- таблица замыканий;

---
<!-- header: 'Хранение иерархических структур / Список соседства' -->
# Список соседства 
Для каждого узла указывается ссылка на его ближайшего предка 

**Например:**
Атрибут parent_id
Для корня parent_id = 0
![bg right contain](./images/neighbor_tree_scheme.svg)

---
![bg contain](./images/neighbor_tree_content.svg)

---
# Получение прямого родителя
Родитель узла "Реляционная (SQL)" (id = 2)
```sql
SELECT * FROM neighbor_tree WHERE id = (
	SELECT parent_id FROM neighbor_tree WHERE id = 2
)
```

---
# Получение всех родителей
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT h1.*, h2.* 
FROM neighbor_tree AS h1 -- первый уровень
	LEFT JOIN neighbor_tree AS h2 -- второй уровень
	ON h1.parent_id = h2.id
	... -- еще уровни
	LEFT JOIN neighbor_tree AS h_N -- уровень N
	ON h_N_prev.parent_id = h_N.id
WHERE h_N.id = 2
```
На каждый уровень в дереве +1 соединение 
Либо рекурсивный CTE 

---
# Получение прямых потомков
Потомки узла "Реляционная (SQL)" (id = 2)
```sql
SELECT * FROM neighbor_tree WHERE parent_id = 2
```

---
# Получение всех потомков
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT h1.*, h2.* 
FROM neighbor_tree AS h1 -- первый уровень
	LEFT JOIN neighbor_tree AS h2 -- второй уровень
	ON h2.parent_id = h1.id
	... -- остальные уровни
WHERE h1.id = 2
```
На каждый уровень в дереве +1 соединение 
Либо рекурсивный CTE 

---

# Вставка листа
Дочерний для "Реляционная (SQL)" (id = 2)
```sql
INSERT INTO neigbor_tree (parent_id, title)
VALUES (2, 'SQLite')
```

---
# Удаление узла
Удаление листа "MySQL" (id = 6)
```sql
DELETE FROM neighbor_tree WHERE id = 6
```
Для удаление узла необходимо переподчинить поддерево удаляемого узла его предку
Удаление узла "Реляционная (SQL)" (id = 2)
```sql
-- запросим предка
SELECT parent_id FROM neighbor_tree WHERE id = 2 -- вернет 1
-- переподчиним поддерево
UPDATE neighbor_tree SET parent_id = 1 WHERE parent_id = 2
-- удалим узел
DELETE FROM neighbor_tree WHERE id = 2
```

---
# Удаление поддерева
**Варианты:**
- удалять узлы на каждом уровне вручную
- использовать ограничение внешего ключа ON DELETE CASCADE

---
# Перемещение узла
Перемещение листа "MariaDB" (id = 7) к узлу "MySQL" (id = 6) 
```sql
UPDATE neighbor_tree SET parent_id = 6 WHERE id = 7
```
Для перемещения узла необходимо переподчинить поддерево перемещаемого узла его предку
Перемещение узла "Постреляционная (NewSQL)" (id = 5) к узлу "Модели данных" (id = 1) 
```sql
-- запросим прямого предка
SELECT parent_id FROM neighbor_tree WHERE id = 5 -- вернет 2
-- переподчиним поддерево
UPDATE neighbor_tree SET parent_id = 2 WHERE parent_id = 5
-- переместим узел
UPDATE neighbor_tree SET parent_id = 1 WHERE id = 5
```

---
# Перемещение поддерева
Перемещение поддерева узла "Постреляционная (NewSQL)" (id = 5) к узлу "Модели данных" (id = 1) 
```sql
UPDATE neighbor_tree SET parent_id = 1 WHERE id = 5
```

---
# Недостатки
Постоянная зависимость от одного родителя
- сложность обслуживания
- сложный просчет глубины

---
# Допустимые способы использования
- необходимо получать только прямых потомков и родителей
- дерево относительно статично
- СУДБ поддерживает рекурсивный CTE

---
<!-- header: 'Хранение иерархических структур / Перечисление путей' -->
# Перечисление путей
Для каждого узла указывается полный путь от корня до самого узла
Путь хранится в строковом типе данных
Идентификаторы узлов в пути перечисляются через разделитель
Путь оканчивается самим узлом и разделителем

![bg right contain](./images/path_enum_scheme.svg)

---
![bg contain](./images/path_enum_content.svg)

---
# Получение прямого родителя
Родитель узла "Реляционная (SQL)" (id = 2)
```sql
SELECT * FROM path_enum WHERE path LIKE (
	SELECT 
		SUBSTRING(
			path, 1, LOCATE(
				'/', REVERSE(
					SUBSTRING(
						path, 1, LENGTH(path)-1
					)
				)
			)
		) 
	FROM path_enum WHERE id = 2
)
```

---
# Получение всех родителей
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT * FROM path_enum 
WHERE (
	SELECT path FROM path_enum WHERE id = 2
) LIKE path || '%'
```

---
# Получение прямых потомков
Потомки узла "Реляционная (SQL)" (id = 2)
```sql
SELECT * FROM path_enum 
WHERE SUBSTRING(
	path, 1, LOCATE(
		'/', REVERSE(
			SUBSTRING(
				path, 1, LENGTH(path)-1
			)
		)
	)
) LIKE (SELECT path FROM path_enum WHERE id = 2)
```

---
# Получение всех потомков
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT * FROM path_enum 
WHERE path LIKE (
	SELECT path || '%' FROM path_enum WHERE id = 2
)
```

---

# Вставка листа
Дочерний для "Реляционная (SQL)" (id = 2)
```sql
-- вставляем узел
INSERT INTO path_enum (title) VALUES ('SQLite')
-- обновляем путь
UPDATE path_enum SET path = (
	SELECT path FROM path_enum WHERE id = 2
) || LAST_INSERT_ID() || '/'
WHERE id = LAST_INSERT_ID()
```

---
# Удаление узла
Удаление листа "MySQL" (id = 6)
```sql
DELETE FROM path_enum WHERE id = 6
```
Для удаление узла необходимо переподчинить поддерево удаляемого узла его предку
Удаление узла "Реляционная (SQL)" (id = 2)
```sql
-- удалим из пути потомков удаляемый узел
UPDATE path_enum SET path = SUBSTRING(
	REPLACE(
		'/2/', CONCAT('/', path), '/'
	), 2
)
-- удалим узел
DELETE FROM path_enum WHERE id = 2
```

---
# Удаление поддерева
Удаление поддерева узла "Реляционная (SQL)" (id = 2)
```sql
DELETE FROM path_enum WHERE id IN (
	SELECT id FROM path_enum 
	WHERE path LIKE (
		SELECT path || '%' FROM path_enum WHERE id = 2
	)	
)
```

---
# Перемещение листа
Перемещение листа "MariaDB" (id = 7) к узлу "MySQL" (id = 6) 
```sql
-- получили прямого предка, id = 2
UPDATE path_enum SET path = (
	SELECT path FROM path_enum WHERE id = 6
) || id || '/'
WHERE id = 7
```

---
# Перемещение узла
Перемещение узла "Постреляционная (NewSQL)" (id = 5) к узлу "Модели данных" (id = 1) 
```sql
-- удалим из пути потомков перемещаемый узел
UPDATE path_enum SET path = SUBSTRING(
	REPLACE(
		'/5/', CONCAT('/', path), '/'
	), 2
)
-- переместим узел
UPDATE path_enum SET path = (
	SELECT path FROM path_enum WHERE id = 1
) || id || '/'
WHERE id = 5
```

---
# Перемещение поддерева
Перемещение поддерева узла "Постреляционная (NewSQL)" (id = 5) к узлу "Модели данных" (id = 1) 
```sql
UPDATE path_enum SET path = REPLACE(
	(SELECT path FROM path_enum WHERE id = 5),
	path,
	(SELECT path FROM path_enum WHERE id = 1)
)
```

---
# Недостатки
- максимальная глубина дерева неявно ограничена максимальным размером типа данных для пути 
- СУБД не может контролировать правильность пути

---
# Допустимые способы использования
- небольшая глубина дерева
- дерево относительно статично

---
<!-- header: 'Хранение иерархических структур / Вложенные множества' -->
# Вложенные множества
Для каждого узла кодируется множество потомков с помощью числовых параметров **left** и **right**

Значение параметра **left** узла должно быть меньше, чем значение left каждого потомка узла
Значение параметра **right** узла должно быть больше, чем значение right каждого потомка узла
![bg right contain](./images/nested_sets_scheme.svg)

---
![bg contain](./images/nested_sets_tree.svg)

---
![bg contain](./images/nested_sets_content.svg)

---
# Получение прямого родителя
Родитель узла "Реляционная (SQL)" (id = 2)
```sql
SELECT parent.*
FROM nested_sets AS h
INNER JOIN nested_sets AS parent
	ON h.lft BETWEEN parent.lft AND parent.rgt
LEFT JOIN nested_sets AS in_between ON 
	(h.lft BETWEEN in_between.lft AND in_between.rgt) AND 
	(in_between.lft BETWEEN parent.lft AND parent.rgt)
WHERE h.id = 2 AND in_between.id IS NULL
```

---
# Получение всех родителей
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT h2.* 
FROM nested_sets AS h1
	INNER JOIN nested_sets AS h2 
	ON h1.lft BETWEEN h2.lft AND h2.rgt
WHERE h1.id = 2
```

---
# Получение прямых потомков
Потомки узла "Реляционная (SQL)" (id = 2)
```sql
SELECT child.*
FROM nested_sets AS h
INNER JOIN nested_sets AS child
	ON child.lft BETWEEN h.lft AND h.rgt
LEFT JOIN nested_sets AS in_between ON 
	(child.lft BETWEEN in_between.lft AND in_between.rgt) AND 
	(in_between.lft BETWEEN child.lft AND child.rgt)
WHERE h.id = 2 AND in_between.id IS NULL
```

---
# Получение всех потомков
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT h2.* 
FROM nested_sets AS h1
	INNER JOIN nested_sets AS h2 
	ON h2.lft BETWEEN h1.lft AND h1.rgt
WHERE h1.id = 2
```

---

# Вставка листа
Дочерний для "Реляционная (SQL)" (id = 2, ltf = 2, rgt = 21)
```sql
-- освобождаем место lft = 3, rgt = 4
UPDATE nested_sets SET lft = 
	CASE WHEN lft>=3
		THEN lft+2 
		ELSE lft 
		END, 
	rgt=rgt+2
WHERE rgt>=20
-- вставляем лист
INSERT INTO nested_sets (title, lft, rgt) VALUES ('SQLite', 3, 4)
```

---
# Удаление узла
Удаление листа "MySQL" (id = 6)
```sql
DELETE FROM nested_sets WHERE id = 6
```
Аналогично для удаления узла - правило построения не нарушается

---
# Удаление поддерева
Удаление поддерева узла "Реляционная (SQL)" (id = 2, ltf = 2, rgt = 21)
```sql
DELETE FROM nested_sets WHERE lft BETWEEN 2 AND 21
```

---
# Перемещение узла
Перемещение листа "MariaDB" (id = 7) к узлу "MySQL" (id = 6, lft = 9, rgt = 10) 
```sql
-- освобождаем место lft = 10, rgt = 11
UPDATE nested_sets SET lft = 
	CASE WHEN lft>=10
		THEN lft+2 
		ELSE lft 
		END, 
	rgt=rgt+2
WHERE rgt>=9
UPDATE nested_sets SET lft = 10, rgt=11 WHERE id = 7
```
Аналогично для перемещения узла - правило построения не нарушается

---
# Перемещение поддерева
Перемещение поддерева узла "Постреляционная (NewSQL)" (id = 5, lft = 15, rgt = 20) к узлу "Нереляционная (NoSQL)" (id = 3, lft = 22, rgt = 43) 
```sql
-- освобождаем место lft = 23, rgt = 28
UPDATE nested_sets SET lft = 
	CASE WHEN lft>=23
		THEN lft+2 
		ELSE lft 
		END, 
	rgt=rgt+5 -- 28-23 == 20-15 == 5
WHERE rgt>=27
-- пересчитываем поддерево
UPDATE nested_sets SET lft = lft+8, rgt=rgt+8 -- 23-15 == 28-20 == 8
WHERE lft BETWEEN 15 AND 20
```

---
# Недостатки
- сложность вставки и перемещения узлов
- ручной контроль целостности дерева

---
# Допустимые способы использования
- необходимо выполнять только запросы для поддеревьев

---
# Возможное улучшение
- хранение для каждого узла его глубины (уровня) в дереве

---
<!-- header: 'Хранение иерархических структур / Таблица замыканий' -->
# Таблица замыканий
Хранение всех путей в дереве
Пары предок-потомок для каждого узла
Атрибуты:
- ancestor - предок
- descendant - потомок
![bg right contain](./images/closure_table_scheme.svg)

---
![bg contain](./images/closure_table_tree.svg)

---
![bg contain](./images/closure_table_content_dict.svg)

---
![bg contain](./images/closure_table_content.svg)

---
# Получение прямого родителя
Родитель узла "Объектно-реляционная" (id = 4)
```sql
SELECT p1.* 
FROM closure_table as p1
LEFT JOIN (
	closure_table as p2 INNER JOIN closure_table as p3 ON p2.parent = p3.children
) ON p2.children = p1.children 
 AND p3.parent = p1.parent 
 AND p2.parent <> p2.children 
 AND p3.parent <> p3.children
WHERE p1.children = 4 and p2.children is NULL;
```

---
# Получение всех родителей
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT h.* 
FROM dbms AS h
INNER JOIN closure_table AS t 
	ON h.id = t.ancestor
WHERE t.descendant = 2
```

---
# Получение прямых потомков
Потомки узла "Реляционная (SQL)" (id = 2)
```sql
SELECT p1.* 
FROM closure_table as p1
LEFT JOIN (
	closure_table as p2 INNER JOIN closure_table as p3 ON p2.children = p3.parent
) ON p2.parent = p1.parent 
 AND p3.children = p1.children 
 AND p2.parent <> p2.children 
 AND p3.parent <> p3.children
WHERE p1.parent = 2 and p2.parent is NULL;
```

---
# Получение всех потомков
Узел "Реляционная (SQL)" (id = 2)
```sql
SELECT h.* 
FROM dbms AS h
INNER JOIN closure_table AS t 
	ON h.id = t.descendant
WHERE t.ancestor = 2
```

---

# Вставка листа
Дочерний для "Реляционная (SQL)" (id = 2)
```sql
INSERT INTO dbms (title) VALUES ('SQLite')
SELECT LAST_INSERT_ID() -- вернет 23
INSERT INTO closure_table (ancestor, descendant)
	SELECT closure_table.ancestor, 23
	FROM closure_table 
	WHERE closure_table.descendant = 2
	UNION ALL 
	SELECT 23, 23
```

---
# Удаление узла
Удаление листа "MySQL" (id = 6)
```sql
DELETE FROM closure_table WHERE descendant = 6
```
Аналогично для удаления узла. Но данные остались в справочнике

---
# Удаление поддерева
Удаление поддерева узла "Реляционная (SQL)" (id = 2)
```sql
DELETE FROM closure_table 
WHERE descendant IN (
	SELECT descendant 
	FROM tree
	WHERE ancestor = 2
)
```

---
# Перемещение поддерева
Перемещение листа "MariaDB" (id = 7) к узлу "MySQL" (id = 6)
+ Отсоединяем поддерево
```sql
DELETE FROM closure_table 
WHERE descendant IN (SELECT descendant 
	FROM tree
	WHERE ancestor = 7)
AND 
ancestor IN (SELECT ancestor 
	FROM tree 
	WHERE descendant = 7 
	AND 	ancestor <> descendant)
```

---
# Перемещение поддерева
Перемещение листа "MariaDB" (id = 7) к узлу "MySQL" (id = 6) 
+ Переносим поддерево
```sql
-- переносим поддерево
INSERT INTO closure_table (ancestor, descendant)
	SELECT super.ancestor, sub.descendant 
	FROM closure_table AS super
	CROSS JOIN closure_table AS sub
WHERE super.descendant = 6
	AND sub.ancestor = 7
```
Аналогично для перемещения узла и листа

---
# Недостатки
- отдельная таблица для хранения взаимосвязей
- логарифмический (?) рост записей в таблице взаимосвязей

---
# Допустимые способы использования
- необходима принадлежность узла нескольким деревьям
- деревья относительно небольшие (объем дискового пространства некритичен)

---
# Возможное улучшение

Добавление атрибута deep
deep = 0 для ссылки узла на себя
deep = 1 для дочерних связей узла
deep = 2 для внучатых связей узла
...

---
<!-- header: 'Хранение иерархических структур / Пара комментариев' -->
# Небольшой комментарий
- для PostgreSQL узнать идентификатор вставленной записи можно с помощью RETURNING
```sql
INSERT INTO tbl(attr1, attr2) VALUES (val1, val2) RETURNING id
```
---
# Еще один комментарий
Существуют дополнительные пакеты и специализированные типы данных различных СУБД, предоставляющих возможности для хранения и обслуживания иерархических структур.
Например, для PostgreSQL:
- array
- ltree (дополнительный пакет)
- ...

---
<!-- header: 'Хранение иерархических структур / Сравнение' -->
![bg contain](./images/comparison_table.svg)

---
<!-- header: '' -->
# Литература
- Б. Карвин, Программирование баз данных SQL. Типичные ошибки и их исправление
- К. Дж. Дейт. Введение в системы баз данных
- Д. Л. Осипов. Технологии проектирования баз данных
- Нормальные формы https://habr.com/ru/post/254773/

---
<!-- _class: lead -->

# Спасибо за внимание
## Вопросы?