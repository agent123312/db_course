---
header: 'Антипаттерны логической стуктуры базы данных'
author: 'Дерюгин Д.Ф.'
_footer: 'Улан-Удэ, БГУ, 2023'
title: Базы данных. Антипаттерны логической стуктуры базы данных. EAV и полиморфная ассоциация
# description: Hosting Marp slide deck on the web
paginate: true
theme: default
_paginate: false

---

<!-- _class: lead -->

## Базы данных
### Антипаттерны логической стуктуры базы данных

# EAV
# Полиморфная ассоциация

#### Дерюгин Д.Ф. 
#### ст. преп. ИСМИ, ИМФКН, БГУ

---
<!-- header: 'Антипаттерны логической стуктуры базы данных / Рабочий пример' -->
# Рабочий пример
## ИС Кофейня
Кофе (цена, единица измерения, название, страна, обжарка)
Специи (цена, единица измерения, название, страна, степень остроты)
Авторский микс (цена, единица измерения, название, автор)
Все это *товары* - но атрибуты *разные*
Точное количество подтипов товаров неизвестно
![bg right contain](./images/pa_classes.svg)


---
<!-- header: 'Антипаттерны логической стуктуры базы данных / EAV' -->
# EAV
## Цель

Поддержка переменных атрибутов

Набор атрибутов сущности разнится от объекта к объекту

Число сабсущностей (подкатегорий) сложно поддается вычислению

---
<!-- header: 'Антипаттерны логической стуктуры базы данных / EAV' -->
Entity-Attribute-Value
Объект-Атрибут-Значение

Поддержка расширяемого решения

Использование таблицы общих атрибутов

Число столбцов неизменно
Нет Null-атрибутов

![bg right contain](./images/eav_scheme.svg)

---
![bg contain](./images/eav_content.svg)

---
# Использование
Класс с динамическим набором атрибутов
- реализация собственных сеттеров и геттеров
- использование переменных как имен атрибутов

---
# Пример. PHP-класс Product
```php
class Product{
    private $values = [];
    public function __get($key){
        if(isset($this->values[$key])){
            return $this->values[$key];
        }
        return NULL;
    }
    public function __set($key, $val){
        $this->values[$key] = $val;
    }
}
```

---
# Работа с классом
```php
$obj = new Product();
$obj->title = 'Название';
$attr_name = 'count';
$obj->$attr_name = 37; // Эквивалент $obj->count = 37
echo $obj->title;
echo $obj->count;
echo $obj->$attr_name;
```

---
# Получение имен всех атрибутов
```php
class Product{
    private $values = [];
    ...
    public function getAttrNames(){
        return array_keys($this->$values);
    }
    ...
}
```

---
# Получение значений всех атрибутов
```php
//Получили в $rows атрибуты объекта из БД;
foreach ($rows as $key => $value) {
    $obj->$key = $value;
}
...
foreach ($obj->getAttrNames() as $key=>$value){
    echo $obj->$key;
    echo '<br>';
}
```

---
# Специальные программные пакеты
- eav-django (Django, Python)
https://django-eav2.readthedocs.io/en/latest/
- eav model for Artisan (Laravel, PHP) 
https://github.com/sunel/eav
- eav (Golang)
https://pkg.go.dev/github.com/corestoreio/pkg/eav
- eav_hashes (Ruby on Rails)
https://github.com/iostat/eav_hashes
- ...

---
# Запрос атрибута
Получение авторов миксов
```sql
SELECT product_id, value
FROM product_attributes
WHERE attribute = 'author'
```

---
# Недостатки
- *невозможно* создать обязательный атрибут
- *невозможно* использовать специальные типы данных
- *невозможно* задавать фиксированные имена атрибутов
- результат запроса **требует** транспонирования
- задачу контроля **выполняет** только код

---
# Допустимые способы использования
- хранение относительно статичных данных
для приложения
- реальная невозможность приведения к
табличному виду для 1-2 таблиц

Если EAV необходим для большего числа таблиц - миграция в сторону NoSQL решений

---
# Как избежать
Моделирование подтипов:
- наследование одиночной таблицы
- наследование конкретной таблицы
- наследование таблицы классов
- слабоструктурированные данные

Мартин Фаулер, "Шаблоны корпоративных приложений"

---
# Наследование одиночной таблицы
Все подтипы хранятся в одной таблице
![bg right contain](./images/single_table.svg)

---
- для не свойственных атрибутов — NULL => разреженная таблица
- необходимо отслеживать применимость атрибутов подтипам
- отптимально, когда подтипов и характерных для них атрибутов мало

---
# Наследование конкретной таблицы
Отдельная таблица на подтип с общими и специфическими атрибутами
![bg right contain](./images/concrete_table.svg)

---
- малая разреженность;
- подтипы отделены друг от друга
- необходимо различать общие атрибуты
- добавление общего атрибута => изменение всех таблиц
- оптимально, когда нет нужды в запросах одновременно на все подтипы

---
# Наследование таблицы классов
Общие атрибуты в базовой таблицы, наследники хранят только свои атрибуты

![bg right contain](./images/class_table.svg)

---
- отсутствует разреженность
- подтипы отделены друг от друга
- гибкое управление общими атрибутами
- оптимально, когда часто приходится искать по всем подтипам

---
# Сериализованный LOB 
Фаулер
Слабоструктурированные данные

Специфические атрибуты в BLOB, JSON, XML, YAML, ...
```json
{
    "country": "Brasil", 
    "roasting": "strong"
    ...
}
```

![bg right contain](./images/blob_table.svg)

---
- полная расширяемость
- сложность поиска по атрибутам подтипов
- большая нагрузка на приложение

---
# Рабочий пример
![bg right contain ](./images/sales.svg)

---
# Полиморфная ассоциация
## Цель 

Ссылка на несколько родительских объектов
product_type - **флаг** родительской таблицы
product_id - могут быть *одинаковые*

---
![bg contain](./images/pa_scheme.svg)

---
# Получение данных о проданном кофе
```sql
SELECT * 
FROM coffee AS c 
INNER JOIN sale AS s ON 
	c.coffee_id = s.product_id AND 
	s.product_type = "coffee"
```

---
# Получение данных о проданных товарах
```sql
SELECT * 
FROM sale AS s 
LEFT JOIN coffee AS c ON c.coffee_id = s.product_id AND s.product_type = "coffee"
LEFT JOIN spice AS sp ON sp.spice_id = s.product_id AND s.product_type = "spice"
LEFT JOIN mix AS m ON m.mix_id = s.product_id AND s.product_type = "mix"
```
+1 соединение на каждый подтип
*Разреженная таблица*

---
# Недостатки
- *невозможно* контролировать целостность
- сложность обслуживания кодом
- большая разреженность результатов запросов

---
# Допустимые способы использования
- среда поддерживает антипаттерн

Например:
- связи vmorph, morphTo (Laravel, PHP)
https://laravel.com/docs/10.x/eloquent-relationships
- gorm (Golang)
https://gorm.io/docs/has_many.html
- django-polymorphic (Django, Python)
https://django-polymorphic.readthedocs.io/en/stable/
- ...

---
# Как избежать
Упрощение отношений
- реверс ссылки
- общая супертаблица

---
# Реверс ссылки
![bg right:65% contain](./images/reverce_link.svg)

---
- если нужно отношение 1:1 — устанавливаем 	ограничение уникальности 
- для просмотра в обоих направлениях —  	используем таблицу пересечений
- запрос по нескольким родительским таблицам — используем UNION с суррогатами NULL

---
# Общая супертаблица
Она же "Таблица классов"
Общие атрибуты в базовой таблицы, наследники хранят только свои атрибуты

---
![bg contain](./images/super_table.svg)

---
<!-- _class: lead -->

# Спасибо за внимание
## Вопросы?